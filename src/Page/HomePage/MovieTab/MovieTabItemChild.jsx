import moment from "moment";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import ModalPopup from "../../../components/ModalPopup/ModalPopup";
import { handleOpenModal } from "../../../redux/Action/ModalAction";

export default function MovieTabItemChild({ dataPhim, tenRap, diaChi }) {
  let dispatch = useDispatch();
  let renderMovieSchedule = () => {
    return (
      <div>
        <h1 className="mb-6">
          <span className="bg-purple-700 text-lg text-white px-1 py-1 rounded-md">
            C18
          </span>
          <span className="text-xl font-medium"> {dataPhim.tenPhim}</span>
        </h1>
        <div className="grid grid-cols-2 gap-5 px-3">
          {dataPhim.lstLichChieuTheoPhim.slice(0, 6).map((schedule) => {
            // Lịch chiếu phòng phim----------------------------
            return (
              <div
                onClick={() => {
                  dispatch(
                    handleOpenModal(
                      true,
                      schedule.ngayChieuGioChieu,
                      diaChi,
                      tenRap,
                      dataPhim.tenPhim
                    )
                  );
                }}
                key={schedule.maLichChieu}
                className=" py-3 cursor-pointer mt-2 font-bold text-center border border-purple-700  text-purple-700 hover:text-white hover:bg-purple-500 duration-300 rounded-lg"
              >
                {moment(schedule.ngayChieuGioChieu).format("DD/MM/yy ~ HH:MM")}
              </div>
            );
          })}
        </div>
      </div>
    );
  };
  let renderMovie = () => {
    return (
      <div className="grid grid-cols-3 px-3 py-8 border-b-2">
        <div className="flex items-center justify-center ">
          <div className="flex items-center justify-center w-5/6 h-80 rounded-md overflow-hidden ">
            <img
              className="w-full hover:w-[500px] h-full hover:h-[105%] duration-100"
              src={dataPhim.hinhAnh}
              alt=""
            />
          </div>
        </div>
        <div className="col-span-2 ">{renderMovieSchedule()}</div>
      </div>
    );
  };
  return <div>{renderMovie()}</div>;
}
