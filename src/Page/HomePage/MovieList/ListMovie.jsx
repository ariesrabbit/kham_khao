import { Pagination } from "antd";
import React, { useEffect, useState } from "react";
import { movieSerVice } from "../../../Service/MovieService";
import { MA_NHOM } from "../../../Service/urlConfig";
import ItemMovie from "./ItemMovie";

export default function ListMovie() {
  let [numberPage, setNumberPage] = useState(1);
  let [totalPage, setotalPage] = useState(1);
  let [listMoviePagination, setListMoviePagination] = useState([]);
  let fetchListMovie = async () => {
    let params = {
      maNhom: MA_NHOM,
      soTrang: numberPage,
      soPhanTuTrenTrang: 8,
    };
    try {
      let res = await movieSerVice.getListMoviePagination(params);
      setotalPage(res.data.content.totalPages);
      setListMoviePagination(res.data.content.items);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchListMovie();
  }, [numberPage]);
  let handleGetNumberPage = (number) => {
    setNumberPage(number);
  };
  let renderListMoviePagination = () => {
    return listMoviePagination.map((itemMovie) => {
      return <ItemMovie key={itemMovie.maPhim} dataMovie={itemMovie} />;
    });
  };
  return (
    <div className="py-32 bg-[#F1F0F7] text-center">
      <div className="container mx-auto">
        {/* // RENDER LIST MOVIE  */}
        <div className="grid grid-cols-4 gap-8 h-[54rem]">
          {renderListMoviePagination()}
        </div>
        <Pagination
          responsive
          size="small"
          className="mt-7 text-2xl space-x-1 "
          onChange={handleGetNumberPage}
          defaultCurrent={1}
          total={totalPage * 10}
        />
      </div>
    </div>
  );
}
