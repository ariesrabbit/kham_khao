import React, { useEffect, useRef, useState } from "react";
import { movieSerVice } from "../../../Service/MovieService";
import { Carousel } from "antd";
import { RightOutlined, LeftOutlined } from "@ant-design/icons";
export default function BannerHomePage() {
  let [listBannerMovie, setListBannerMovie] = useState([]);
  let carouselRef = useRef();
  let fetchBanner = async () => {
    try {
      let res = await movieSerVice.getBannerMovie();
      setListBannerMovie(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchBanner();
  }, []);
  let renderBannerListMovie = () => {
    return listBannerMovie.map((bannerMovie) => {
      return (
        <div className="h-[29rem]" key={bannerMovie.maBanner}>
          <img className="w-full h-full" src={bannerMovie.hinhAnh} alt="" />
        </div>
      );
    });
  };
  return (
    <div>
      <Carousel ref={carouselRef} autoplay className="relative">
        {renderBannerListMovie()}
      </Carousel>
      <button
        onClick={() => {
          carouselRef.current.next();
        }}
        className="absolute  top-1/4 right-4  text-[5rem]  text-[#9e9e9e] hover:text-[#91A9B3] font-bold transition z-10"
      >
        <RightOutlined />
      </button>
      <button
        onClick={() => {
          carouselRef.current.prev();
        }}
        className="absolute top-1/4 left-4  text-[5rem]  text-[#9e9e9e] hover:text-[#91A9B3] font-bold transition z-10"
      >
        <LeftOutlined />
      </button>
    </div>
  );
}
