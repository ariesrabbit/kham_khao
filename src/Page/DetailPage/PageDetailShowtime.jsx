import { Tabs, Collapse, Rate, Modal } from "antd";
import moment from "moment/moment";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Lottie from "lottie-react";
import sr_login from "../../asset/sr_loading.json";
import PageDetailMap from "./PageDetailMap";
import PageDetailComment from "./PageDetailComment";
import { useDispatch, useSelector } from "react-redux";
import ModalPopup from "../../components/ModalPopup/ModalPopup";
import { handleOpenModal } from "../../redux/Action/ModalAction";

const { Panel } = Collapse;
const CustomTabs = styled(Tabs)`
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: #aa00ff !important;
  }
  .ant-tabs-tab:hover {
    color: #e254ff !important;
  }
  .ant-tabs-ink-bar {
    position: absolute;
    background: #7200ca;
    pointer-events: none;
  }
`;

export default function PageDetailShowtime({
  showTime,
  scrollRefMuaVe,
  tenPhim,
}) {
  console.log("showTime: ", showTime);
  let dispatch = useDispatch();
  let renderLichChieuPhim = (lichChieuPhim, tenRap, diaChi) => {
    return lichChieuPhim.map((schedule) => {
      return (
        <div>
          <div
            onClick={() => {
              dispatch(
                handleOpenModal(
                  true,
                  schedule.ngayChieuGioChieu,
                  diaChi,
                  tenRap,
                  tenPhim
                )
              );
            }}
            key={schedule.maLichChieu}
            className="w-1/4 py-3 mt-2 font-bold text-center border border-purple-700  text-purple-700 hover:text-white hover:bg-purple-500 duration-300 rounded-lg cursor-pointer"
          >
            {moment(schedule.ngayChieuGioChieu).format("DD/MM/yy ~ HH:MM")}
          </div>
        </div>
      );
    });
  };
  // RENDER THEATER ============================
  let renderTheater = (cumRapChieu, logo) => {
    return cumRapChieu?.map((cumRap) => {
      return (
        <Panel
          header={
            <div className="flex justify-around w-full">
              <div className="w-1/12 flex items-center">
                <img src={logo} alt="" />
              </div>
              <div className="w-10/12 text-left  border-b">
                <div className="text-xl">{cumRap.tenCumRap}</div>
                <div className="flex items-center justify-between text-sm space-x-2">
                  <span>{cumRap.diaChi}</span> <PageDetailMap />
                  <span></span>
                </div>
              </div>
            </div>
          }
          key={cumRap.maCumRap}
        >
          <div className="h-full overflow-auto">
            {renderLichChieuPhim(
              cumRap.lichChieuPhim,
              cumRap.tenCumRap,
              cumRap.diaChi
            )}
          </div>
        </Panel>
      );
    });
  };
  // RENDER CONTENT ===============================
  let renderContent = () => {
    if (showTime.heThongRapChieu.length !== 0) {
      return showTime.heThongRapChieu?.map((heThongRap) => {
        return (
          <Tabs.TabPane
            tab={
              <div className=" p-2 border border-purple-700 rounded-lg ">
                <img className="w-8 h-8" src={heThongRap.logo} />
              </div>
            }
            key={heThongRap.maHeThongRap}
          >
            <Collapse
              accordion
              expandIconPosition="end"
              style={{ height: "30rem" }}
              defaultActiveKey="1"
            >
              {renderTheater(heThongRap.cumRapChieu, heThongRap.logo)}
            </Collapse>
          </Tabs.TabPane>
        );
      });
    } else {
      return (
        <div className="container text-center text-xl font-semibold mb-3">
          <div className="w-1/3 mx-auto">
            <Lottie animationData={sr_login} />
            Hiện chưa có lịch chiếu cho phim này !!!
          </div>
        </div>
      );
    }
  };
  return (
    <div>
      <div className="container mx-auto py-8">
        <h1 ref={scrollRefMuaVe} className="mb-5 text-xl font-bold ">
          Lịch chiếu {showTime.tenPhim}
        </h1>
        <div>
          {showTime.dangChieu ? (
            // {/* // SHOW PHIM DANG CHIEU ======================== */}
            <div className="flex pb-10  border ">
              <div className="w-2/3">
                <CustomTabs
                  tabBarGutter={"2rem"}
                  tabBarExtraContent={{
                    left: <div className="w-8"></div>,
                  }}
                  tabPosition="top"
                  defaultActiveKey="BHDStar"
                >
                  {renderContent()}
                </CustomTabs>
              </div>
              {/* // CARD ĐÁNH GIÁ  ==================================== */}
              <div className="w-1/3 flex   justify-center">
                <div className="w-auto  text-center mt-20 h-96 mx-auto rounded shadow-lg border ">
                  <Lottie className="" animationData={sr_login} />
                  <div className="py-6">
                    <PageDetailComment rate={showTime.danhGia} />
                  </div>
                </div>
              </div>
            </div>
          ) : (
            // SHOW PHIM CHƯA CHIẾU ===========================================
            <div className="container text-center  text-xl font-semibold mb-3">
              <div className="w-1/3 mx-auto">
                <Lottie animationData={sr_login} />
                Phim sẽ sớm ra mắt trong thời gian tới !!!
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
