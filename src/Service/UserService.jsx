import axios from "axios";
import { BASE_URL, configHeader, https } from "./urlConfig";

export const userService = {
  postUser: (data) => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
    //   method: "POST",
    //   data: data,
    //   headers: configHeader(),
    // });
    let uri = "/api/QuanLyNguoiDung/DangNhap";
    return https.post(uri, data);
  },
};
