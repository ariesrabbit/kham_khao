import { combineReducers } from "redux";
import { CommentReducer } from "./CommentReducer";
import { LoadingReducer } from "./LoadingReducer";
import { ModalReducer } from "./ModalReducer";
import UserReducer from "./UserReducer";

export const rootReducer = combineReducers({
  UserReducer,
  LoadingReducer,
  CommentReducer,
  ModalReducer,
});
