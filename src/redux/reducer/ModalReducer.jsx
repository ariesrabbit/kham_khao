import { OPEN_MODAL } from "../constant/ModalPopupConstant";

const initialState = {
  dataModal: {
    isOpen: false,
    thoiGian: "",
    diaChi: "",
    tenRap: "",
    tenPhim: "",
  },
};

export let ModalReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case OPEN_MODAL: {
      state.dataModal.isOpen = payload.value;
      state.dataModal.thoiGian = payload.thoiGian;
      state.dataModal.diaChi = payload.diaChi;
      state.dataModal.tenRap = payload.tenRap;
      state.dataModal.tenPhim = payload.tenPhim;
      return { ...state };
    }

    default:
      return state;
  }
};
