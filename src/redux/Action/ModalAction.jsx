import { OPEN_MODAL } from "../constant/ModalPopupConstant";

export let handleOpenModal = (value, thoiGian, diaChi, tenRap, tenPhim) => {
  return {
    type: OPEN_MODAL,
    payload: { value, thoiGian, diaChi, tenRap, tenPhim },
  };
};
