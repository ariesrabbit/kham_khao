import React, { useState } from "react";
import { Button, Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { handleOpenModal } from "../../redux/Action/ModalAction";
import moment from "moment/moment";

export default function ModalPopup() {
  let dispatch = useDispatch();
  let isModalOpen = useSelector((state) => {
    return state.ModalReducer.dataModal.isOpen;
  });
  let dataModal = useSelector((state) => {
    return state.ModalReducer.dataModal;
  });

  console.log("modal hiện ra");
  return isModalOpen ? (
    <div>
      <Modal
        centered
        title={dataModal.tenPhim}
        open={isModalOpen}
        footer={<Button>Chọn ghế và thanh toán</Button>}
        onCancel={() => {
          dispatch(handleOpenModal(false));
        }}
      >
        <p>Rạp chọn : {dataModal.diaChi}</p>
        <p>Ngày chiếu: {moment(dataModal.thoiGian).format("DD/MM/yy")}</p>
        <p>Giờ chiếu: {moment(dataModal.thoiGian).format("HH:MM")}</p>
        <p>Địa chỉ: {dataModal.diaChi}</p>
      </Modal>
    </div>
  ) : null;
}
